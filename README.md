# ecGEM_tutorial

Repository including tutorials for the use of enzyme-constrained genome scale metabolic models (Practical 1) and batch simulations using dynamic flux balance analysis (Practical 2). Jupyter notebook with missing code and with complete solutions are provided.

Models were obtained from Lu, H., Li, F., Sánchez, B. J., Zhu, Z., Li, G., Domenzain, I., ... & Nielsen, J. (2019). A consensus S. cerevisiae metabolic model Yeast8 and its ecosystem for comprehensively probing cellular metabolism. Nature communications, 10(1), 3586.

Experimental data was obtained from Hanly, T. J., Urello, M., & Henson, M. A. (2012). Dynamic flux balance modeling of S. cerevisiae and E. coli co-cultures for efficient consumption of glucose/xylose mixtures. Applied microbiology and biotechnology, 93, 2529-2541.

Scripts are based on Moreno‐Paz, S., Schmitz, J., Martins dos Santos, V. A., & Suarez‐Diez, M. (2022). Enzyme‐constrained models predict the dynamics of Saccharomyces cerevisiae growth in continuous, batch and fed‐batch bioreactors. Microbial Biotechnology, 15(5), 1434-1445.